###############################################################################
# (c) Copyright 2020-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "LICENSE".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os
import zipfile
from datetime import timedelta
from io import BytesIO
from unittest.mock import MagicMock, Mock, call, patch

from lb.nightly.configuration import Project, Slot
from pytest import raises

from lb.nightly.functions.common import get_build_method
from lb.nightly.functions.rpc import LockTakenError, build, checkout

from .test_checkout import working_directory


@patch(
    "lb.nightly.functions.common.MAX_TIME_WAITING_FOR_DEPLOYMENT", timedelta(seconds=3)
)
@patch(
    "lb.nightly.functions.common.TIME_BETWEEN_DEPLOYMENT_CHECKS", timedelta(seconds=1)
)
@patch(
    "lb.nightly.functions.common.deployments_ready",
)
@patch(
    "lb.nightly.functions.rpc.download_and_unzip",
)
@patch(
    "shutil.copyfileobj",
)
@patch(
    "lb.nightly.functions.build.singularity_run",
)
@patch(
    "lb.nightly.functions.common.run",
)
@patch(
    "lb.nightly.functions.common.singularity_run",
)
@patch(
    "lb.nightly.configuration.Project.get_deployment_directory",
)
@patch(
    "lb.nightly.utils.Repository.FileRepository.pull",
)
@patch(
    "lb.nightly.functions.rpc.service_config",
    return_value={"gitlab": {"token": "some_token"}, "artifacts": {"uri": "artifacts"}},
)
@patch(
    "lb.nightly.configuration.service_config",
    return_value={"gitlab": {"token": "some_token"}, "artifacts": {"uri": "artifacts"}},
)
@patch(
    "lb.nightly.functions.rpc.get",
    return_value=Slot(
        "test-slot", build_id=123, projects=[Project("Moore", "HEAD")]
    ).Moore,
)
@patch("lb.nightly.functions.rpc.db")
@patch(
    "lb.nightly.utils.Repository.FileRepository.exist",
)
def test_build_full(
    artifact_exist,
    db,
    get,
    service_config_main,
    service_config,
    pull,
    get_deployment_directory,
    singularity_run,
    run,
    buildrun,
    copy,
    down_unz,
    wait_for_deploy,
    tmp_path,
):
    copy = MagicMock()
    down_unz.return_value = True
    wait_for_deploy.return_value = True
    pull.return_value = BytesIO()
    singularity_run.return_value.returncode = 0
    run.return_value.returncode = 0
    buildrun.return_value.returncode = 0
    buildrun.return_value.stderr = b""
    platform = "x86_64-centos7-gcc9-opt"
    # Case 1: artifact does not exist, noone else builds it, we've got a lock
    artifact_exist.return_value = False

    with patch("json.dump"), working_directory(tmp_path):
        report = build("flavour/slot/123/project", platform, "")
    service_config.assert_called_once()
    service_config_main.assert_called()
    get.assert_called_once()
    artifact_name = "build/Moore/6a/6a43480a4cf2a50d2d9e84dbb3b5cad3005aa4995b6375049fde38dbf04212f6.zip"
    db.assert_has_calls(
        [call().lock(artifact_name.replace("/", ":"), info={"artifact": artifact_name})]
    )
    buildrun.assert_called()
    archive_path = tmp_path / os.path.basename(artifact_name)
    assert zipfile.is_zipfile(archive_path)
    with zipfile.ZipFile(archive_path) as archive:
        names = archive.namelist()
        assert f"Moore/.logs/{platform}/build/report.json" in names
        assert f"Moore/.logs/{platform}/build/report.md" in names
    artifact_path = tmp_path / "artifacts" / artifact_name
    assert os.path.exists(artifact_path)
    assert open(archive_path, "rb").read() == open(artifact_path, "rb").read()

    # Case 1a: missing deployment directories
    wait_for_deploy.return_value = False
    with raises(RuntimeError):
        with patch("json.dump"), working_directory(tmp_path):
            report = build("flavour/slot/123/project", platform, "")
    wait_for_deploy.return_value = True

    # Case 2: artifact exists
    db.reset_mock()
    artifact_exist.reset_mock()
    buildrun.reset_mock()
    get.reset_mock()
    service_config.reset_mock()
    service_config_main.reset_mock()
    artifact_exist.return_value = True

    with patch("json.dump"), working_directory(tmp_path):
        report = build("flavour/slot/123/project", platform, "")

    service_config.assert_called_once()
    service_config_main.assert_called()
    get.assert_called_once()
    db.assert_has_calls([db()])
    buildrun.assert_not_called()
    assert (
        f"Summary for the build artifact for Moore/HEAD and x86_64-centos7-gcc9-opt updated in the database"
        in str(report["script"].records)
    )

    # Case 3: artifact exists, but summary not available
    db.reset_mock()
    dbins = db.return_value
    dbins.reuse_artifact.return_value = False
    artifact_exist.reset_mock()
    buildrun.reset_mock()
    get.reset_mock()
    service_config.reset_mock()
    service_config_main.reset_mock()
    artifact_exist.return_value = True

    with patch("json.dump"), working_directory(tmp_path):
        report = build("flavour/slot/123/project", platform, "")

    service_config.assert_called_once()
    service_config_main.assert_called()
    get.assert_called_once()
    db.assert_has_calls([db()])
    buildrun.assert_not_called()
    assert (
        f"Summary for the build artifact for Moore/HEAD and x86_64-centos7-gcc9-opt not found in the database"
        in str(report["script"].records)
    )

    # Case 4: artifact does not exist, someone else builds it, we can't get a lock
    artifact_exist.reset_mock()
    buildrun.reset_mock()
    get.reset_mock()
    service_config.reset_mock()
    service_config_main.reset_mock()
    artifact_exist.return_value = False
    db.reset_mock()
    db.side_effect = [LockTakenError()]
    with patch("json.dump"), working_directory(tmp_path):
        report = build("flavour/slot/123/project", platform, "")
    service_config.assert_called_once()
    service_config_main.assert_called()
    get.assert_called_once()
    buildrun.assert_not_called()
    assert (
        f"Someone else building Moore/HEAD on {platform} (artifact: {artifact_name}). Exiting"
        in str(report["script"].records)
    )
