###############################################################################
# (c) Copyright 2020-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "LICENSE".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import logging
import os
import zipfile
from datetime import timedelta
from io import BytesIO
from pathlib import Path
from unittest.mock import MagicMock, Mock, patch

import pytest
from lb.nightly.configuration import Project, Slot

from lb.nightly.functions.common import get_build_method
from lb.nightly.functions.rpc import test as script_test

from .test_checkout import working_directory


@patch(
    "lb.nightly.functions.common.MAX_TIME_WAITING_FOR_DEPLOYMENT", timedelta(seconds=3)
)
@patch(
    "lb.nightly.functions.common.TIME_BETWEEN_DEPLOYMENT_CHECKS", timedelta(seconds=1)
)
@patch(
    "lb.nightly.functions.common.deployments_ready",
)
@patch(
    "shutil.copyfileobj",
)
@patch(
    "lb.nightly.functions.build.singularity_run",
)
@patch(
    "lb.nightly.functions.common.singularity_run",
)
@patch(
    "lb.nightly.utils.Repository.FileRepository.pull",
)
@patch(
    "lb.nightly.functions.rpc.service_config",
    return_value={"gitlab": {"token": "some_token"}, "artifacts": {"uri": "artifacts"}},
)
@patch(
    "lb.nightly.configuration.service_config",
    return_value={"gitlab": {"token": "some_token"}, "artifacts": {"uri": "artifacts"}},
)
@patch(
    "lb.nightly.functions.rpc.get",
    return_value=Slot(
        "test-slot", build_id=123, projects=[Project("Moore", "HEAD")]
    ).Moore,
)
@patch("lb.nightly.functions.rpc.db")
@patch(
    "lb.nightly.functions.rpc.download_and_unzip",
)
def test_test_full(
    download_unzip,
    db,
    get,
    service_config_main,
    service_config,
    pull,
    singularity_run,
    buildrun,
    copy,
    wait_for_deploy,
    tmp_path,
):
    download_unzip.return_value = True
    wait_for_deploy.return_value = True
    copy.return_value = None
    pull.return_value = BytesIO()
    singularity_run.return_value.returncode = 0
    buildrun.return_value.returncode = 0
    platform = "x86_64-centos7-gcc9-opt"
    with patch("json.dump"), working_directory(tmp_path):
        Path(tmp_path / "Moore").mkdir()
        Path(tmp_path / "Moore/build").mkdir()
        Path(tmp_path / "Moore/build/CMakeCache.txt").touch()
        script_test("flavour/slot/123/project", platform, "")

    service_config.assert_called_once()
    service_config_main.assert_called()
    get.assert_called_once()
    db.assert_has_calls([db(), db()])
    buildrun.assert_called()

    artifact_name = "test/Moore/6a/6a43480a4cf2a50d2d9e84dbb3b5cad3005aa4995b6375049fde38dbf04212f6.zip"
    archive_path = tmp_path / os.path.basename(artifact_name)
    assert zipfile.is_zipfile(archive_path)
    with zipfile.ZipFile(archive_path) as archive:
        names = archive.namelist()
        assert f"Moore/.logs/{platform}/test/report.json" in names
        assert f"Moore/.logs/{platform}/test/report.md" in names
    artifact_path = tmp_path / "artifacts" / artifact_name
    assert os.path.exists(artifact_path)
    assert open(archive_path, "rb").read() == open(artifact_path, "rb").read()
