###############################################################################
# (c) Copyright 2020-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "LICENSE".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from lb.nightly.configuration import Project, Slot

from lb.nightly.functions.build import cmake, cmt
from lb.nightly.functions.common import get_build_method


def test_get_build_method():

    p = Project("a", "v1r0", env=["proj=a"])
    slot = Slot(
        "test",
        projects=[p],
    )
    assert get_build_method(p) == cmake
    p = Project("a", "v1r0", build_tool="cmt")
    slot = Slot(
        "test",
        projects=[p],
    )
    assert get_build_method(p) == cmt
    p = Project("a", "v1r0")
    slot = Slot("test", projects=[p], build_tool="cmt")
    assert get_build_method(p) == cmt
    p = Project("a", "v1r0", build_tool="cmt")
    slot = Slot("test", projects=[p], build_tool="cmake")
    assert get_build_method(p) == cmt
