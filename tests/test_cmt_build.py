###############################################################################
# (c) Copyright 2020-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "LICENSE".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os
from unittest.mock import patch

from lb.nightly.configuration import Project

from lb.nightly.functions.build import cmt

from .test_checkout import working_directory


@patch(
    "lb.nightly.functions.build.singularity_run",
)
def test_build(singularity_run, caplog, tmp_path):
    singularity_run.return_value.returncode = 0
    os.environ["CMTCONFIG"] = "x86_64-centos7-gcc9-opt"
    os.environ["BINARY_TAG"] = os.environ["CMTCONFIG"]
    with working_directory(tmp_path):
        result, _ = cmt(Project("Gaudi", "master")).build()
    assert "command exited with code 0" in str(result.records)
    assert "cmt run make all" in result.command
