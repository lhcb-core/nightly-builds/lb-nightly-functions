###############################################################################
# (c) Copyright 2020-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "LICENSE".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os
from unittest.mock import patch

from lb.nightly.configuration import Project

from lb.nightly.functions.build import cmake_old

from .test_checkout import working_directory


@patch(
    "lb.nightly.functions.build.singularity_run",
)
def test_build(singularity_run, caplog, tmp_path):
    singularity_run.return_value.returncode = 0
    with working_directory(tmp_path):
        result, _ = cmake_old(Project("Gaudi", "master")).build(env={})
    for target in (
        "configure",
        "all",
        "unsafe-install",
        "post-install",
        "clean",
    ):
        assert f"make BUILDDIR=build {target}" in result[target].command
        assert result[target].returncode == 0
