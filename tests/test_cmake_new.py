###############################################################################
# (c) Copyright 2020-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "LICENSE".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import datetime
import os
from platform import platform
from shlex import quote
from shutil import which
from unittest.mock import patch

import pytest
from configurator.config import Config
from lb.nightly.configuration import Project, Slot

from lb.nightly.functions.build import cmake_new
from lb.nightly.functions.common import compute_env, get_singularity_root

from .test_checkout import working_directory

patch_settings = patch(
    "lb.nightly.configuration.service_config",
)


@pytest.mark.parametrize("project", (Project("Gaudi", "master"),))
def test_prepare_cache(tmp_path, project):
    with working_directory(tmp_path):
        build = cmake_new(project)
        cache_entries = {
            "CMAKE_USE_CCACHE": True,
        }
        build._prepare_cache(cache_entries=cache_entries)
        cache = """set(CMAKE_USE_CCACHE "True" CACHE STRING "override")
set(CMAKE_C_COMPILER_LAUNCHER ccache CACHE STRING "override")
set(CMAKE_CXX_COMPILER_LAUNCHER ccache CACHE STRING "override")
set(CMAKE_RULE_LAUNCH_COMPILE "lb-wrapcmd <CMAKE_CURRENT_BINARY_DIR> <TARGET_NAME>"
    CACHE STRING "override")
set(CMAKE_RULE_LAUNCH_LINK "lb-wrapcmd <CMAKE_CURRENT_BINARY_DIR> <TARGET_NAME>"
    CACHE STRING "override")
set(CMAKE_RULE_LAUNCH_CUSTOM "lb-wrapcmd <CMAKE_CURRENT_BINARY_DIR> <TARGET_NAME>"
    CACHE STRING "override")
"""
        cache_file = build._cache_preload_file()
        assert os.path.exists(cache_file)
        with open(cache_file) as f:
            assert cache == f.read()


@pytest.mark.parametrize("project", (Project("Gaudi", "master"),))
def test_env(project):
    env = {"key": "value"}
    build = cmake_new(project)
    buildenv = compute_env(project, init_env=env)
    assert "key" in buildenv and "value" == buildenv["key"]


@patch("lb.nightly.functions.build.singularity_run")
def test_strip_kwargs(run):
    run.return_value.returncode = 0
    project = Project("Gaudi", "master")
    build = cmake_new(project)
    build._run(["pwd"], env={"BINARY_TAG": "x86_64-centos7-gcc9-opt"})
    for arg in cmake_new.SPECIAL_ARGS:
        assert arg not in compute_env(project)


@pytest.mark.skipif(
    not os.path.exists("/cvmfs/cernvm-prod.cern.ch") or not which("singularity"),
    reason="requires cvmfs and singularity",
)
@patch_settings
def test_run(mock_settings):
    build = cmake_new(Project("Gaudi", "master"))
    cmd = ["echo", "Hello World!"]
    platform = "x86_64-centos7-gcc9-opt"
    cmd_kwargs = {
        "step": "configure",
        "env": {
            "BINARY_TAG": platform,
            "SINGULARITY_ROOT": get_singularity_root(platform),
        },
    }
    mock_settings.return_value = Config({})

    build._run(cmd, **cmd_kwargs)

    assert "running echo Hello World!" in str(build.reports["configure"])
    assert "command exited with code 0" in str(build.reports["configure"])
    assert isinstance(build.reports["configure"].returncode, int)
    assert build.reports["configure"].returncode >= 0
    assert build.reports["configure"].command == " ".join(quote(a) for a in cmd)


@pytest.mark.skipif(
    not os.path.exists("/cvmfs/cernvm-prod.cern.ch") or not which("singularity"),
    reason="requires cvmfs and singularity",
)
@patch_settings
@patch(
    "lb.nightly.functions.build.datetime",
    **{"now.return_value": datetime.datetime(2020, 7, 17, 15, 34, 25, 854889)},
)
def test_started_completed(date_mock, mock_settings):
    build = cmake_new(Project("Gaudi", "master"))
    cmd = ["echo", "'Hello World!'"]
    platform = "x86_64-centos7-gcc9-opt"
    mock_settings.return_value = Config({})
    build._run(
        ["pwd"],
        env={
            "BINARY_TAG": platform,
            "SINGULARITY_ROOT": get_singularity_root(platform),
        },
    )
    assert b"Start: 2020-07-17T15:34:25.854889" in build.reports["script"].stdout
    assert b"End: 2020-07-17T15:34:25.854889" in build.reports["script"].stdout


@patch(
    "lb.nightly.functions.build.find_path",
    **{
        "return_value": os.path.join(
            "lcg-toolchains",
            "LCG_97a",
            "x86_64-centos7-gcc9-opt.cmake",
        )
    },
)
@patch("lb.nightly.functions.build.singularity_run", **{"return_value.returncode": 0})
def test_lcg_slot(run, path_mock, tmp_path):
    gaudi = Project("Gaudi", "master")
    lcg = Project("LCG", "97a")
    slot = Slot("my-slot", projects=[gaudi, lcg])
    build = cmake_new(gaudi)
    with working_directory(tmp_path):
        env = {}
        env["BINARY_TAG"] = "x86_64-centos7-gcc9-opt"
        build.configure(env=env)
        assert (
            f"-DCMAKE_TOOLCHAIN_FILE={path_mock.return_value}"
            in build.reports["configure"].command
        )


@patch(
    "lb.nightly.functions.build.find_path",
    **{
        "return_value": os.path.join(
            "lcg-toolchains",
            "LCG_97a",
            "x86_64-centos7-gcc9-opt.cmake",
        )
    },
)
@patch("lb.nightly.functions.build.singularity_run", **{"return_value.returncode": 0})
def test_lcg_env(run, path_mock, tmp_path):
    gaudi = Project("Gaudi", "master")
    build = cmake_new(gaudi)
    with working_directory(tmp_path):
        env = {}
        env["LCG_VERSION"] = "97a"
        env["BINARY_TAG"] = "x86_64-centos7-gcc9-opt"
        build.configure(env=env)
        assert (
            f"-DCMAKE_TOOLCHAIN_FILE={path_mock.return_value}"
            in build.reports["configure"].command
        )


def test_no_lcg():
    with pytest.raises(EnvironmentError):
        gaudi = Project("Gaudi", "master")
        build = cmake_new(gaudi)
        build.configure(env={})


@patch(
    "lb.nightly.functions.build.find_path",
    **{"return_value": None},
)
def test_no_toolchain(path_mock, tmp_path):
    with pytest.raises(FileNotFoundError), working_directory(tmp_path):
        gaudi = Project("Gaudi", "master")
        env = {}
        env["LCG_VERSION"] = "97a"
        env["BINARY_TAG"] = "x86_64-centos7-gcc9-opt"
        build = cmake_new(gaudi)
        build.configure(env=env)


@patch(
    "lb.nightly.functions.build.find_path",
    **{
        "return_value": os.path.join(
            "lcg-toolchains",
            "LCG_97a",
            "x86_64-centos7-gcc9-opt.cmake",
        )
    },
)
@patch("lb.nightly.functions.build.singularity_run", **{"return_value.returncode": 0})
def test_build(run, path_mock, tmp_path):
    gaudi = Project("Gaudi", "master")
    with working_directory(tmp_path):
        env = {}
        env["PATH"] = ""
        env["CMAKE_PREFIX_PATH"] = ""
        env["LCG_VERSION"] = "97a"
        env["BINARY_TAG"] = "x86_64-centos7-gcc9-opt"
        reps, _ = cmake_new(gaudi).build(env=env)
        assert (
            f"-DCMAKE_TOOLCHAIN_FILE={path_mock.return_value}"
            in reps["configure"].command
        )


@patch(
    "lb.nightly.functions.build.find_path",
    **{
        "return_value": os.path.join(
            "lcg-toolchains",
            "LCG_97a",
            "x86_64-centos7-gcc9-opt.cmake",
        )
    },
)
@patch(
    "lb.nightly.functions.build.singularity_run",
)
def test_configuration_failed(run, toolchain_mock, tmp_path):
    run.return_value.returncode = 1
    env = {}
    env["PATH"] = ""
    env["CMAKE_PREFIX_PATH"] = ""
    env["LCG_VERSION"] = "97a"
    env["BINARY_TAG"] = "x86_64-centos7-gcc9-opt"
    with working_directory(tmp_path):
        result, _ = cmake_new(Project("Gaudi", "master")).build(env=env)
    assert result["configure"].returncode == 1
    assert not hasattr(result["build"], "returncode")


@patch(
    "lb.nightly.functions.build.find_path",
    **{
        "return_value": os.path.join(
            "lcg-toolchains",
            "LCG_97a",
            "x86_64-centos7-gcc9-opt.cmake",
        )
    },
)
@patch(
    "lb.nightly.functions.build.singularity_run",
)
def test_build_ok(run, toolchain_mock, tmp_path):
    with working_directory(tmp_path) as curdir:
        run.return_value.returncode = 0
        env = {}
        env["LCG_VERSION"] = "97a"
        env["BINARY_TAG"] = "x86_64-centos7-gcc9-opt"
        result, _ = cmake_new(Project("Gaudi", "master")).build(env=env)
        for step in ["configure", "build", "install"]:
            assert result[step].returncode == 0
        assert (
            f"cmake -S Gaudi -B Gaudi/build "
            f"-G Ninja -C Gaudi/build/cache_preload.cmake "
            "-DCMAKE_TOOLCHAIN_FILE=lcg-toolchains/LCG_97a/x86_64-centos7-gcc9-opt.cmake"
        ) in result["configure"].command
        assert f"cmake --build Gaudi/build" in result["build"].command
        assert (
            f"cmake --install Gaudi/build --prefix Gaudi/InstallArea/x86_64-centos7-gcc9-opt"
            in result["install"].command
        )
